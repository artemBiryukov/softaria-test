package application;

import java.net.URL;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * This class is used for check changes between two tables and configuring template to send to EmailSender class.
 */
public class ChangesChecker {

    public static String checkChanges(Hashtable<URL, String> yesterdayTable, Hashtable<URL, String> todayTable) {
        StringBuilder added = new StringBuilder("2) Появились следующие страницы: \n");
        StringBuilder changed = new StringBuilder("3) Изменились следующие страницы: \n");
        StringBuilder deleted = new StringBuilder("1) Исчезли следующие страницы: \n");
        StringBuilder resultMessage = new StringBuilder();
        HashSet<URL> commonKeys = new HashSet<>();

        commonKeys.addAll(todayTable.keySet());
        commonKeys.addAll(yesterdayTable.keySet());

        for (URL url : commonKeys) {
            if (!todayTable.containsKey(url)) {
                deleted.append(url).append('\n');
            } else if (!yesterdayTable.containsKey(url)) {
                added.append(url).append('\n');
            } else {
                if (!todayTable.get(url).equals(yesterdayTable.get(url))) {
                    changed.append(url).append('\n');
                }
            }
        }
        resultMessage.append(deleted).append(added).append(changed);
        return resultMessage.toString();
    }
}
