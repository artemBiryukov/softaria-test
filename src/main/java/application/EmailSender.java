package application;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class is used for creating message and sending email to address from config.properties file.
 * Also config.properties is used to set other properties for email server and user credentials.
 */
public class EmailSender {
    private static Logger logger = Logger.getLogger(EmailSender.class);

    public static void sendEmail(String msg) {
        StringBuilder sb = new StringBuilder()
                .append("Здравствуйте, дорогая и.о. секретаря\n\n")
                .append("За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n")
                .append(msg)
                .append("\nС уважением,\n\n").append("автоматизированная система мониторинга.");

        Properties properties = System.getProperties();
        try {
            InputStream configProperties = EmailSender.class.getResourceAsStream("/config.properties");
            properties.load(configProperties);
            configProperties.close();
            final String user = properties.getProperty("user");
            final String password = properties.getProperty("password");
            final String messageTo = properties.getProperty("messageTo");
            Session session = Session.getDefaultInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password);
                }
            });
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(messageTo));
            message.setSubject("Без темы");
            message.setText(sb.toString());
            Transport.send(message);
            logger.info("Sending message...");
        } catch (IOException e) {
            logger.error("Error in loading properties.", e);
        } catch (MessagingException e) {
            logger.error("Error in sending Email.", e);
        }
    }
}
