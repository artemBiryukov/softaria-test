package application;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * This class is used for read/write operations with files.
 */
public class FilesManager {
    private static Logger logger = Logger.getLogger(FilesManager.class);

    public static void writeFile(String s, File file) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(s);
            writer.flush();
            logger.info("Writing file...");
        } catch (IOException e) {
            logger.error("Error in writing file.", e);
        }
    }

    public static String readFile(File file) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            logger.info("Reading file...");
            while (reader.ready()) {
                sb.append(reader.readLine());
            }
        } catch (IOException e) {
            logger.error("Error in reading file.", e);
        }
        return sb.toString();
    }

}
