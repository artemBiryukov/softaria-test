package application;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Just a simple class for reading HTML string from given URL.
 */
public class UrlReader {
    private static Logger logger = Logger.getLogger(UrlReader.class);

    public static String readUrl(URL url) {
        StringBuilder sb = new StringBuilder();
        try {
            logger.info("Reading URL...");
            InputStream inputStream = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while (reader.ready()) {
                sb.append(reader.readLine());
            }
            reader.close();
            inputStream.close();
        } catch (IOException e) {
            logger.error("Error in reading URL.");
        }
        return sb.toString();
    }
}
