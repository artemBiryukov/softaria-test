package application;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Properties;

/**
 * This class purpose is to read info from Json files, put it into tables, check changes and write today URLs
 * into yesterday URLs file. CRON variable sets time interval between updates.
 */
@Component
public class UrlTracker {
    private Hashtable<URL, String> yesterdayTable = new Hashtable<>();
    private Hashtable<URL, String> todayTable = new Hashtable<>();
    private static Properties properties = new Properties();
    private Logger logger = Logger.getLogger(UrlTracker.class);
    //Временной интервал обновления в милисекундах
    private static final long CRON = 60000;

    /**
     * Files format ["url1", "url2", ...]
     */
    public @Scheduled(fixedRate = CRON)
    void updateUrlsAndSendMail() {
        try {
            logger.info("Updating...");

            InputStream configProperties = getClass().getResourceAsStream("/config.properties");
            BufferedReader configReader = new BufferedReader(new InputStreamReader(configProperties));
            properties.load(configReader);
            configReader.close();
            configProperties.close();
            File todayJsonFile = new File(properties.getProperty("pathToTodayFile"));
            File yesterdayJsonFile = new File(properties.getProperty("pathToYesterdayFile"));
            yesterdayTable.clear();
            todayTable.clear();
            Gson gson = new Gson();

            String today = FilesManager.readFile(todayJsonFile);
            String yesterday = FilesManager.readFile(yesterdayJsonFile);
            URL[] urlsToday = gson.fromJson(today, URL[].class);
            URL[] urlsYesterday = gson.fromJson(yesterday, URL[].class);

            if (urlsToday != null) {
                Arrays.stream(urlsToday).forEach(url -> todayTable.put(url, UrlReader.readUrl(url)));
            }
            if (urlsYesterday != null) {
                Arrays.stream(urlsYesterday).forEach(url -> yesterdayTable.put(url, UrlReader.readUrl(url)));
            }

            String changes = ChangesChecker.checkChanges(yesterdayTable, todayTable);
            EmailSender.sendEmail(changes);

            //Обновление файла yesterdayJsonFile значениями из todayJsonFile
            FilesManager.writeFile(gson.toJson(todayTable.keySet().toArray()), yesterdayJsonFile);
            logger.info("Succesfully updated!");
        } catch (IOException e) {
            logger.error("Error in reading config.properties");
        }

    }
}
